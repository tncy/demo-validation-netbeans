package net.tncy.demo.validation;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author Xavier Roy
 */
public class Demo {

    public static void main(String[] args) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Calendar calendar = Calendar.getInstance();
        calendar.set(1974, Calendar.DECEMBER, 3);
        Date birthDate = calendar.getTime();
        Person p = new Person("Xavier", "Roy", birthDate, "FR");
        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        for (ConstraintViolation<Person> violation : violations) {
            System.out.println(violation.getMessage());
        }
    }
}
